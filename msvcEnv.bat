@echo off

set MSVC_ENV_USAGE=Usage: msvc_env [/generate] [arch]
set MSVC_ENV_TOOL_DIR=%~dp0
set hcd_platform=vs14.0

set msvc_env_mode=load
set msvc_env_arch=amd64
set msvc_env_arch_set=false



:label_parse_arguments

if [%1]==[] (
  goto label_no_further_arguments
)

set msvc_env_argument=%1
shift

if [%msvc_env_argument:~0,1%]==[/] (
  set msvc_env_argument=%msvc_env_argument:~1%
) else if [%msvc_env_argument:~0,2%]==[--] (
  set msvc_env_argument=%msvc_env_argument:~2%
) else if [%msvc_env_arch_set%]==[false] (
  set msvc_env_arch_set=true
  set msvc_env_arch=%msvc_env_argument%
  goto label_parse_arguments
) else (
  echo ERROR - unexpected argument: %msvc_env_argument%
  echo %MSVC_ENV_USAGE%
  exit /B 1
)

if [%msvc_env_argument%]==[generate] (
  set msvc_env_mode=generate
) else (
  echo ERROR - unknown option: /%msvc_env_argument%
  echo %MSVC_ENV_USAGE%
  exit /B 1
)

goto label_parse_arguments




:label_no_further_arguments

set env_file=%MSVC_ENV_TOOL_DIR%\%hcd_platform%-%msvc_env_arch%.env

if [%msvc_env_mode%]==[load] (
  goto label_load_env
) else if [%msvc_env_mode%]==[generate] (
  goto label_generate
)

echo ERROR: unexpected mode: %msvc_env_mode%
exit /B 1




:label_load_env

if exist %env_file% (
  for /f "tokens=*" %%a in (%env_file%) do (
    set %%a
  )
) else (
  echo ERROR: No file %env_file%
  echo Unsupported arch %msvc_env_arch%?
  exit /B 1
)

goto end




:label_generate

echo generating %env_file%

if [%msvc_env_arch%]==[amd64] (
  call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" amd64
) else if [%msvc_env_arch%]==[x86] (
  call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x86
) else (
  echo ERROR: Unsupported arch "%msvc_env_arch%"
  exit /B 1
)

set >%env_file%

goto end


:end
exit /B 0
